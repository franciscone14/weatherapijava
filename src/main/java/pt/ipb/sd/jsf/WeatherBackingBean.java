package pt.ipb.sd.jsf;

import pt.ipb.sd.ejb.LocationManager;
import pt.ipb.sd.ejb.LocationManagerRemote;
import pt.ipb.sd.ejb.WeatherManager;
import pt.ipb.sd.ejb.WeatherManagerRemote;
import pt.ipb.sd.entity.Location;
import pt.ipb.sd.entity.Weather;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.List;

@Named
@RequestScoped
public class WeatherBackingBean {

    @EJB
    private WeatherManagerRemote weatherManager;

    @EJB
    private LocationManagerRemote locationManager;

    private String weather;
    private String description;
    private float temperature;
    private long locationId;

    public List<Weather> getWeathers(){ return weatherManager.getWeathers(); }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public String doCreateWeather(){
        Weather weather = new Weather(getWeather(), getDescription(), getTemperature());
        locationManager.addWeatherLocation(getLocationId(), weather);
        return "weathers";
    }
}
