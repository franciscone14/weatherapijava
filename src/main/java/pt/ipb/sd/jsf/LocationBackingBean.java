package pt.ipb.sd.jsf;

import pt.ipb.sd.ejb.LocationManagerLocal;
import pt.ipb.sd.ejb.LocationManagerRemote;
import pt.ipb.sd.entity.Location;
import pt.ipb.sd.entity.Weather;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.List;

@Named
@RequestScoped
public class LocationBackingBean {

    @EJB
    private LocationManagerLocal locationManager;

    private long id;
    private String city;
    private String country;
    private String unit;

    public List<Location> getLocations(){
        List<Location> list = locationManager.getLocations();
        return list;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String doCreateLocation(){
        locationManager.create(getCity(), getCountry(), getUnit());
        return "locations";
    }
}
