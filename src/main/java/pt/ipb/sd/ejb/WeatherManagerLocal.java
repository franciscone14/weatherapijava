package pt.ipb.sd.ejb;

import pt.ipb.sd.entity.Location;
import pt.ipb.sd.entity.Weather;

import javax.ejb.Local;
import java.util.List;

@Local
public interface WeatherManagerLocal {
    Weather create(String weather, String description, float temperature);

    List<Weather> getWeathers();

    Weather getById(long id);

    List<Weather> getByLocation(Location location);

    Weather update(Weather weather);

    void delete(Weather weather);

    void delete(long id);
}
