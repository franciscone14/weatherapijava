package pt.ipb.sd.ejb;

import pt.ipb.sd.entity.Location;
import pt.ipb.sd.entity.Weather;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Stateless
public class LocationManager implements LocationManagerLocal, LocationManagerRemote {

    @PersistenceContext(unitName = "weathers-pu")
    private EntityManager entityManager;

    @Override
    public Location create(String city, String country, String unit) {
        Location location = new Location(city, country, unit);
        entityManager.persist(location);
        return location;
    }

    @Override
    public List<Location> getLocations() {
        List<Location> list = new ArrayList<>();
        List<Object[]> list1 = entityManager
                .createNativeQuery("SELECT * FROM WEATHER W RIGHT JOIN LOCATIONWEATHERS L ON " +
                        "W.ID = L.WEATHERS_ID RIGHT JOIN LOCATION L2 on L.LOCATION_ID = L2.ID").getResultList();

        for (Object[] obj: list1) {
            boolean notFound = true;
            for(Location l: list){
                if(l.getId() == Integer.parseInt(obj[6].toString()) && obj[0] != null){
                    Weather weather = new Weather(obj[3].toString(), obj[1].toString(),Float.parseFloat(obj[2].toString()));
                    weather.setId(Integer.parseInt(obj[0].toString()));
                    l.addWeather(weather);
                }
            }
            if(notFound){
                Location location = new Location(obj[7].toString(), obj[8].toString(), obj[9].toString());
                location.setId(Long.parseLong(obj[6].toString()));
                location.setWeathers(new ArrayList<>());
                if(obj[0] != null){
                    Weather weather = new Weather(obj[3].toString(), obj[1].toString(),Float.parseFloat(obj[2].toString()));
                    weather.setId(Integer.parseInt(obj[0].toString()));
                    location.addWeather(weather);
                }
                list.add(location);
            }
        }
        return list;
    }

    @Override
    public Location getLocationById(long id) {
        Location location = null;
        List<Object[]> list1 = entityManager
                .createNativeQuery("SELECT * FROM WEATHER W RIGHT JOIN LOCATIONWEATHERS L ON " +
                        "W.ID = L.WEATHERS_ID RIGHT JOIN LOCATION L2 on L.LOCATION_ID = L2.ID AND L2.ID = " + id).getResultList();

        for (Object[] obj: list1) {
            if(location == null){
                location = new Location(obj[7].toString(), obj[8].toString(), obj[9].toString());
                location.setId(Long.parseLong(obj[6].toString()));
                location.setWeathers(new ArrayList<>());
            }
            if(obj[0] != null){
                Weather weather = new Weather(obj[3].toString(), obj[1].toString(),Float.parseFloat(obj[2].toString()));
                weather.setId(Integer.parseInt(obj[0].toString()));
                location.addWeather(weather);
            }
        }
        return location;
    }

    @Override
    public Location addWeatherLocation(long id, Weather weather) {
        Location l = entityManager.find(Location.class, id);
        entityManager.persist(weather);
        l.addWeather(weather);
        return l;
    }

    @Override
    public Location update(Location location) {
        return entityManager.merge(location);
    }

    @Override
    public void delete(Location location) {
        entityManager.remove(location);
    }

    @Override
    public void delete(long id) {
        Location location = entityManager.find(Location.class, id);
        entityManager.remove(location);
    }
}
