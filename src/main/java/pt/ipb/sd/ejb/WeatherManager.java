package pt.ipb.sd.ejb;

import pt.ipb.sd.entity.Location;
import pt.ipb.sd.entity.Weather;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/***
 * Weather does not have a state to be saved and changed in the run time
 */
@Stateless
public class WeatherManager implements WeatherManagerLocal, WeatherManagerRemote {

    @PersistenceContext(unitName = "weathers-pu")
    private EntityManager entityManager;

    @Override
    public Weather create(String weather, String description, float temperature) {
        Weather weather1 = new Weather(weather, description, temperature);
        entityManager.persist(weather1);
        return weather1;
    }

    @Override
    public List<Weather> getWeathers() {
        List<Weather> l = entityManager.createNamedQuery(Weather.ALL, Weather.class).getResultList();
        return l;
    }

    @Override
    public Weather getById(long id) {
        return entityManager.find(Weather.class, id);
    }

    @Override
    public List<Weather> getByLocation(Location location) {
        Query query = entityManager.createNativeQuery(
                "SELECT w.ID, w.WEATHER, w.DESCRIPTION, w.TEMPERATURE FROM Weather w JOIN LOCATIONWEATHERS lw ON " +
                        "lw.LOCATION_ID = " + location.getId());

        List<Object[]> objects = query.getResultList();
        System.out.println(objects.get(0)[0]);

        List<Weather> l = null;
//        System.out.println("SIZEEEE: " + l.size());
        return l;
    }

    @Override
    public Weather update(Weather weather) { return entityManager.merge(weather); }

    @Override
    public void delete(Weather weather) { entityManager.remove(weather); }

    @Override
    public void delete(long id) {
        Weather weather = entityManager.find(Weather.class, id);
        entityManager.remove(weather);
    }
}
