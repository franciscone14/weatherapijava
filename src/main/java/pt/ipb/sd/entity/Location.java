package pt.ipb.sd.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

//SELECT c1, c2 FROM Country c1 INNER JOIN c1.neighbors c2

@Entity
@NamedQueries({
        @NamedQuery(name = Location.ALL2, query = "SELECT l, w FROM Location l LEFT JOIN l.weathers w"),
        @NamedQuery(name = Location.ALL, query = "SELECT l FROM Location l"),
        @NamedQuery(name = Location.COUNT_ALL, query = "SELECT COUNT(l) FROM Location l")

})
public class Location implements Serializable {
    //Queries
    public final static String ALL = "Location.ALL";
    public final static String COUNT_ALL = "Location.COUNT_ALL";
    public final static String ALL2 = "Location.ALL2";
    public static final long serialVersionUID = 42L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    private String city;
    private String country;
    private String unit;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="LOCATIONWEATHERS")
    private List<Weather> weathers = new ArrayList<>();

    public Location(String city, String country, String unit) {
        this.city = city;
        this.country = country;
        this.unit = unit;
    }

    public Location(){
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() { return country; }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

//    public List<Weather> getWeathers() {
//        return weathers;
//    }
//
    public void setWeathers(List<Weather> weathers) {
        this.weathers = weathers;
    }

    public void addWeather(Weather weather){
        this.weathers.add(weather);
    }

    @Override
    public String toString(){
        String str = city + " - " + country + "[";
        for (Weather w: weathers) {
            str += w.getDescription() + ", ";
        }
        str += "]";
        return str;
    }
}
