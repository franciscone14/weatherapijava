package pt.ipb.sd.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NamedQueries({
    @NamedQuery(name = Weather.ALL, query = "SELECT w FROM Weather w"),
    @NamedQuery(name = Weather.COUNT_ALL, query = "SELECT COUNT(w) FROM Weather w"),
    @NamedQuery(name = Weather.FILTER, query = "SELECT w FROM Weather w JOIN LOCATIONWEATHERS lw WHERE lw.location_id = :location_id")
})
public class Weather implements Serializable {

    //Queries
    public final static String ALL = "Weather.ALL";
    public final static String COUNT_ALL = "Weather.COUNT_ALL";
    public static final String FILTER = "Weather.FILTER";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    private String weather;
    private String description;
    private float temperature;

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "location_id")
//    @JsonIgnore
//    private Location location;

    public Weather(String weather, String description, float temperature) {
        this.weather = weather;
        this.description = description;
        this.temperature = temperature;
    }

    public Weather() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

//    public Location getLocation() {
//        return location;
//    }
//
//    public void setLocation(Location location) {
//        this.location = location;
//    }
}
