package pt.ipb.sd.rest;

import pt.ipb.sd.entity.Weather;
import pt.ipb.sd.ejb.WeatherManagerRemote;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/api/weathers")
public class WeatherRest {

    @EJB
    WeatherManagerRemote weatherManager;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Weather get(@PathParam("id") long id){
        return weatherManager.getById(id);
    }

    @DELETE
    @Path("{id}")
    public void deleteWeatherById(@PathParam("id") long id){
        weatherManager.delete(id);
    }
}
