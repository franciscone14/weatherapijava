package pt.ipb.sd.rest;

import pt.ipb.sd.ejb.LocationManagerRemote;
import pt.ipb.sd.ejb.WeatherManager;
import pt.ipb.sd.ejb.WeatherManagerRemote;
import pt.ipb.sd.entity.Location;
import pt.ipb.sd.entity.Weather;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/api/locations")
public class LocationRest {

    @EJB
    LocationManagerRemote locationManager;
    @EJB
    WeatherManagerRemote weatherManager;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Location> list(){
        List<Location> locations = locationManager.getLocations();
        return locations;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(Location l){
        Location newLocation = locationManager.create(l.getCity(), l.getCountry(), l.getUnit());
        return Response.status(200).entity(newLocation).build();
    }

    @POST
    @Path("/{id}/weathers")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createLocationWeather(@PathParam("id") long id, Weather w){
        Location l = locationManager.addWeatherLocation(id, w);
        return Response.status(200).entity(l).build();
    }

    @GET
    @Path("/{id}/weathers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Weather> getLocationWeather(@PathParam("id") long id){
        System.out.println("FUCKKKKKKKKKKK: " + id);
        Location l = locationManager.getLocationById(id);
        System.out.println(l.getCountry());
        return weatherManager.getByLocation(l);
    }

    @DELETE
    @Path("{id}")
    public void deleteLocationById(@PathParam("id") long id){
        locationManager.delete(id);
    }

    @GET
    @Path("{id}")
    public Location getById(@PathParam("id") long id){
        return locationManager.getLocationById(id);
    }
}
